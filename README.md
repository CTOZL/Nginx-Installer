# Nginx Installer


#### 介绍


本脚本程序是使用脚本自动化将nginx源码在CentOS系统下编译安装的程序。

内置nginx在linux编译时需要的pcre、pcre-devel、zlib、zlib-devel依赖工具包，

nginx软件的源码压缩包，openssl的源码压缩包（本脚本在nginx编译时包含了编译ssl模块）。


#### 程序目录结构说明
```
Nginx_Installer
│
├─dependence_tools---------------依赖工具包及编译扩展模块存放目录
│  │
│  ├─openssl-------------------------openssl模块源码
│  │
│  ├─pcre	-------------------------编译需要用到的pcre依赖包
│  │
│  └─zlib	-------------------------编译需要用到的zlib依赖包
│
└─nginx_src----------------------Nginx源码存放目录
```

#### 使用说明
1、使用FTP工具将本程序包上传到服务器（解压后的本程序文件夹请勿带空格）

2、使用cd 命令切换到install_nginx.sh脚本所在的文件夹下

3、使用sh install_nginx.sh 命令执行，直到程序结束即可
