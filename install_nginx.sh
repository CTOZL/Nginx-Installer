﻿#!/bin/bash

curdir="$(pwd)"
echo "当前目录为:$curdir"
echo "编译安装的nginx版本为:nginx-1.13.7"


#安装编译源码的依赖工具pcre pcre-devel
rpm -ivh $curdir/dependence_tools/pcre/pcre-8.32-17.el7.i686.rpm
rpm -ivh $curdir/dependence_tools/pcre/pcre-8.32-17.el7.x86_64.rpm
rpm -ivh $curdir/dependence_tools/pcre/pcre-devel-8.32-17.el7.x86_64.rpm
echo "编译工具pcre、pcre-deve 安装完毕..."


#安装编译源码的依赖工具zlib zlib-devel
rpm -ivh $curdir/dependence_tools/zlib/zlib-1.2.7-17.el7.i686.rpm
rpm -ivh $curdir/dependence_tools/zlib/zlib-1.2.7-17.el7.x86_64.rpm
rpm -ivh $curdir/dependence_tools/zlib/zlib-devel-1.2.7-17.el7.x86_64.rpm
echo "编译工具zlib、zlib-devel 安装完毕..."

#解压nginx源码至临时目录
rm -rf /tmp/nginx-1.13.7
tar -xzf $curdir/nginx_src/nginx-1.13.7.tar.gz -C /tmp
echo "nginx-1.13.7源码 解压完毕..."

#解压openssl源码至临时目录
rm -rf /tmp/openssl-1.1.0g
tar -xzf $curdir/dependence_tools/openssl/openssl-1.1.0g.tar.gz -C /tmp
echo "openssl-1.1.0g源码 解压完毕..."

#初始化nginx编译配置(包含ssl模块)
cd /tmp/nginx-1.13.7
./configure --prefix=/usr/local/nginx-1.13.7 --with-http_stub_status_module --with-http_gzip_static_module --with-http_ssl_module --with-openssl=/tmp/openssl-1.1.0g
echo "nginx-1.13.7 编译配置完毕..."

make
echo "nginx-1.13.7 编译完毕..."

make install
echo "nginx-1.13.7 安装完毕...安装目录为: /usr/local/nginx-1.13.7"

#切换回安装脚本所在目录
cd $curdir